# Docker Image: `skullyka/hello-world-flask`

## Description

This Docker image is designed for demonstration purposes, specifically for testing and validating the operational status of the demo platform. It features a basic Flask web application that responds with "Hello, World!" when accessed. The application is lightweight and serves as an ideal tool for verifying that web services within your environment are functioning correctly.

The source code and project details are available on GitLab: [https://gitlab.com/skullyka/hello-world-flask](https://gitlab.com/skullyka/hello-world-flask).

Docker Hub Repository: [skullyka/hello-world-flask](https://hub.docker.com/r/skullyka/hello-world-flask)

## Features

- **Simple Flask Application:** The core of this image is a minimal Flask application. It's designed to be lightweight and easy to deploy, perfect for testing environments.
  
- **Customizable Port:** The application listens on a port specified by the `PORT` environment variable. This allows for flexible deployment, catering to environments where specific port assignments are necessary.

- **Quick Deployment:** Being a minimal setup, this image can be pulled and deployed rapidly, making it ideal for environments where quick testing is required.

- **Publicly Accessible:** Hosted on Docker Hub, this image can be easily pulled using Docker commands.

## Usage

To pull and run this Docker image:

```bash
docker pull skullyka/hello-world-flask
docker run -p [YourPort]:[YourPort] -e PORT=[YourPort] skullyka/hello-world-flask
```
Replace [YourPort] with the port number you wish to use.

## Tags

latest: The latest build of the "Hello World" Flask application.

v1, v2, ... : Versioned tags for specific iterations of the application.

## Disclaimer

This image is intended for demonstration and testing purposes only. It's not designed for production use and lacks features necessary for a secure and scalable production environment.

